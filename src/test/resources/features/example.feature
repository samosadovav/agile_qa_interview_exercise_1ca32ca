Feature: Number of vowels and consonant
  As a user I want to be able to add a list of strings.
  I want to see the number of vowels in each of the strings that I have entered.

  Scenario Outline: Return from [<string>] number of vowels and consonant
    When I add <string> with <separator>
    Then then i should see number of vowels and consonant

    Examples:
      | string               | separator |
      | qwerty astring sring | SPACE     |
      | 123                  | SPACE     |
      | abcd123              | SPACE     |


#    Possible test cases:
#  1. Case sensitive tests
#  2. Max string length
#  3. Complicated string such as test-test
#  4. 1 char String
#  5. Alphabet test (e.g. our application accept only English)
#  6. More then 4 strings
#  7. String with special characters
#  8. with only vowels
#  9. with only consonant
#  10. test with duplicate strings
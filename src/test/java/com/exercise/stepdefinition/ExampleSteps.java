package com.exercise.stepdefinition;


import com.exercise.model.ResultModel;
import com.exercise.model.Separator;
import cucumber.api.java8.En;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static com.exercise.stepdefinition.StepUtils.extractedVowelsAndConsonants;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ExampleSteps implements En {
    List<ResultModel> result = new ArrayList<>();

    public ExampleSteps() {
        When("^I add (.*) with (.*)$", (String string, Separator separator) -> {

            assertThat(string.length()).overridingErrorMessage("String is empty").isNotZero();
            assertThat(string)
                    .overridingErrorMessage("String [%s] doesn't contains vowels or consonant", string)
                    .containsPattern("[a-zA-Z]");

            String[] split = string.split(separator.value());
            for (String str : split) {
                result.add(extractedVowelsAndConsonants(str));
            }
        });

        Then("^then i should see number of vowels and consonant$", () -> {
            for (ResultModel resultModel : result) {
                log.info("String = {} contains vowels = {}   consonants = {}\n",
                        resultModel.getString(),
                        resultModel.getVowels(),
                        resultModel.getConsonants());
            }
        });
    }
}

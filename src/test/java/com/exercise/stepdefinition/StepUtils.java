package com.exercise.stepdefinition;

import com.exercise.model.ResultModel;

public class StepUtils {

    public static  ResultModel extractedVowelsAndConsonants(String str) {
        int vowels = 0, consonants = 0;
        for (int i = 0; i < str.length(); ++i) {
            char ch = str.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                ++vowels;
            } else if ((ch >= 'a' && ch <= 'z')) {
                ++consonants;
            }
        }
        return ResultModel.builder()
                .string(str)
                .vowels(vowels)
                .consonants(consonants).build();
    }
}

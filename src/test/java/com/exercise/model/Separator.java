package com.exercise.model;

public enum Separator {
    SPACE(" "),
    COMMA(", ");

    Separator(String value) {
        this.value = value;
    }

    private final String value;

    public String value() {
        return value;
    }
}

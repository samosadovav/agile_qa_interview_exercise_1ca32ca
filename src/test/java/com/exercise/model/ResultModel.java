package com.exercise.model;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultModel {
    private String string;
    private int vowels;
    private int consonants;
}

Feature: Number of vowels and consonant
  As a user I want to be able to add a list of strings.
  I want to see the number of vowels in each of the strings that I have entered.

  Scenario Outline: Return from [<string>] number of vowels and consonant
    When I add <string> with <separator>
    Then then i should see number of vowels and consonant

    Examples:
      | string               | separator |
      | qwerty astring sring | SPACE     |
      | 123                  | SPACE     |
      | abcd123              | SPACE     |


#    Possible test cases:
#  1.case sensitive tests
